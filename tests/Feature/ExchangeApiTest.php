<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class ExchangeApiTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();

        Http::fake([
            'https://api.exchangeratesapi.io/latest' => Http::response([
                "EUR" => 1.0950743555,
                "date" => "2020-09-18",
                "exchange_advice" => "no"
            ], 200),
        ]);
    }

    public function testExchangeEndpointReturnsOK()
    {
        $response = $this->get('api/v1/exchange');

        $response->assertStatus(200);
    }

    public function testExchangeEndpointReturnsGbpToEurByDefault()
    {
        $response = $this->getJson('api/v1/exchange');
        $response->assertStatus(200)
                    ->assertJsonStructure([
                        "EUR",
                        "date",
                        "exchange_advice"
                    ])
                    ->assertJson([
                        "EUR" => 1.0950743555,
                        "date" => "2020-09-18",
                        "exchange_advice" => "no"
                    ]);
    }
}
