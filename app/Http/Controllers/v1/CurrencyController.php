<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Services\v1\ExchangeContract;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CurrencyController extends Controller
{
    protected $exchange;

    public function __construct(ExchangeContract $exchange)
    {
        $this->exchange = $exchange;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = request()->input();
        $from = isset($params['from']) ? $params['from'] : 'GBP';
        $to = isset($params['to']) ? $params['to'] : 'EUR';

        $latestExchange = $this->exchange->latest($from, $to);
        $lastWeekDate = Carbon::parse($latestExchange['date'])->subWeek()->format('Y-m-d');
        $lastWeekExchange = $this->exchange->historic($lastWeekDate, $from, $to);

        $advice = reset($latestExchange) < reset($lastWeekExchange) ? 'yes' : 'no';
        $latestExchange['exchange_advice'] = $advice;

        return response($latestExchange);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
