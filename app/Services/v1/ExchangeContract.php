<?php

namespace App\Services\v1;

interface ExchangeContract
{
    public function latest($fromCurrency, $toCurrency = ''): array;

    public function historic($date, $fromCurrency, $toCurrency = ''): array;
}