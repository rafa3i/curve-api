<?php

namespace App\Services\v1;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;

class ExchangeRatesApi implements ExchangeContract
{
    protected $baseUri = 'https://api.exchangeratesapi.io/';

    public function latest($fromCurrency, $toCurrency = ''): array
    {
        $response = Http::get($this->baseUri.'latest', [
            'base'    => $fromCurrency,
            'symbols' => $toCurrency
        ]);

        $jsonDecLatest = json_decode($response->body());

        $latestRate = $jsonDecLatest->rates->$toCurrency;

        return $latest = [
            $toCurrency => $latestRate,
            'date' => $jsonDecLatest->date,
        ];
    }

    public function historic($date, $fromCurrency, $toCurrency = ''): array
    {
        $response = Http::get($this->baseUri.$date, [
            'base'    => $fromCurrency,
            'symbols' => $toCurrency
        ]);

        $jsonDecHistoric = json_decode($response->body());
        $historicRate = $jsonDecHistoric->rates->$toCurrency;

        return $historic = [
            $toCurrency => $historicRate,
            'date' => $date
        ];
    }
}