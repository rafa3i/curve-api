<?php

namespace App\Providers;

use App\Services\v1\ExchangeContract;
use App\Services\v1\ExchangeRatesApi;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ExchangeContract::class, function() {
            switch (Config::get('app.exchange_provider')) {
                case 'exchange_rates_api':
                    return new ExchangeRatesApi();
                default:
                    throw new \RuntimeException("Unknown currency api");
            }
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
